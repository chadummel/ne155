function [A,b] = NE155_HW5_1a(n)
% Written by Chad Ummel for MATLAB R2012a
% Input:  n
% In the homework problem, n is given as 100.  However, just for fun, I've
% made n an adjustable input, so the code will produce tridiagonal matrices
% of size nxn.  To satisfy the homework problem, use
% [A,b] = NE_155_HWW5_1a(100)
% Outputs:  A:  an nxn square matrix.
%           b:  a n-length column vector

% Initialize A and b
A = zeros(n,n);
b = zeros(n,1);

% Construct first row of A and first entry of b using given info.
A(1,1) = 2;
A(1,2) = -1;
b(1) = 0;

% Construct rows 2 through n-1 of A and entries 2 through n-1 of b.
for j = 1:n-2
    A(1+j,j) = -1;
    A(1+j,j+1) = 2;
    A(1+j,j+2) = -1;
    b(1+j) = j;
end

% Construct final row of A and final entry of b using given info.
A(n,n-1) = -1;
A(n,n) = 2;
b(n) = n-1;

end
function [x,k,errorabs,errorrel] = NE155_HW5_4c(n,w,errormetric,tol)
% Written by Chad Ummel for MATLAB R2012a
% Inputs:   n:              The number of elements in x, b, and in the nxn
%                           matrix, A.
%           w:              Omega in the Successive Over Relaxation
%                           iterative method algorithm.
%           errormetric:    A string specifying the desired metric used to
%                           compute the iterative error.
%                           'absolute' - the absolute error
%                           'relative' - the relative error
%           tolerance:      The specified tolerance on the iterated
%                           solution
% Outputs:  x:          The iterative solution vector found using the SOR
%                       iterative method within the inputted tolerance
%           k:          The number of iterations required to reach the
%                       desired tolerance
%           errorabs:   The absolute error of the final iterated solution
%           errorrel:   The relative error of the final iterated solution

% For Problem 4, use
% [x,k,errorabs,errorrel] = NE155_HW5_4c(5,1.1,'absolute',1e-6)
% For Problem 5, use
% [x,k,errorabs,errorrel] = NE155_HW5_4c(5,1.1,'relative',tol), where
% tol = 1e-6 or 1e-8

% Construct A and b in the same method used for Problem 1a
A = zeros(n,n);
b = 100*ones(n,1);
A(1,1) = 4;
A(1,2) = -1;
for j = 1:n-2
    A(1+j,j) = -1;
    A(1+j,j+1) = 4;
    A(1+j,j+2) = -1;
end
A(n,n-1) = -1;
A(n,n) = 4;

% Create a matrix, x.  The top row of x will be x^(0) which will be equal
% to the zero vector.  The second row of x will be x^(1) and so forth.
x = zeros(1,n);

% Find the real solution to Ax = b and use the 2-norm to find the absolute
% error
xreal = inv(A)*b;
errorabs = sqrt(sum((x'-xreal).^2));

% Initialize the relative error as infinity.
errorrel = Inf;

% Use the inputted desired error metric
if strcmp(errormetric,'absolute') == 1
    lim = errorabs;
elseif strcmp(errormetric,'relative') == 1
    lim = errorrel;
end

% Initialize the iteration number
k = 0;

% Loop until we meet a tolerance of less than 1e-6
while lim > tol
    
    % Increase the iteration number by one for each loop.
    k = k + 1;
    
    % Initialize the iterated x as a row of zeros under the previous x.
    x = [x;zeros(1,n)];
    
    % Loop through each element of x
    for i = 1:n
        
        % Initialize the second and third terms of the Gauss Seidel method
        % algorithm
        sumleft = 0;
        sumright = 0;
        
        % Compute the terms.  Note that we use x(end,j) for sum left
        % instead of x(end-1,j), which we used for the Jacobi method.
        for j = 1:i-1
            sumleft = sumleft + A(i,j)*x(end,j);
        end
        for j = i+1:n
            sumright = sumright + A(i,j)*x(end-1,j);
        end
        
        % Compute each element of the newly formed x
        x(end,i) = (1-w)*x(end-1,i) + w*(100 - sumleft - sumright)/A(i,i);
    end
    
    % Recompute the error
    errorabs = sqrt(sum((x(end,:)'-xreal).^2));
    errorrel = sqrt((sum((x(end,:)-x(end-1,:)).^2))/sum((x(end,:)).^2));
    if strcmp(errormetric,'absolute') == 1
        lim = errorabs;
    elseif strcmp(errormetric,'relative') == 1
        lim = errorrel;
    end
    
end

% Remove the solution (the last row of the x matrix) and transpose it to
% output as a column vector
x = x(end,:)';

end
function [conditionnumber] = NE155_HW5_1b
% Written by Chad Ummel for MATLAB R2012a
% Output:  conditionnumber:  The condition number of matrix A.

% Construct A and b using the code from part a.
[A,b] = NE155_HW5_1a(100);

% Find the inverse of A.
invA = inv(A);

% Take the 2 norm of A and its inverse.  They should both be symmetrical,
% so A is equal to its transpose (as is invA).
twonormA = sqrt(max(eig(A*A)));
twonorminvA = sqrt(max(eig(invA*invA)));

% Find the condition number.
conditionnumber = twonormA*twonorminvA;

end
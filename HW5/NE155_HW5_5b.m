function [wopt] = NE155_HW5_5b(n,errormetric,tol)
% Written by Chad Ummel for MATLAB R2012a
% Inputs:   n:              The number of elements in x, b, and in the nxn
%                           matrix, A.
%           errormetric:    A string specifying the desired metric used to
%                           compute the iterative error.
%                           'absolute' - the absolute error
%                           'relative' - the relative error
%           tolerance:      The specified tolerance on the iterated
%                           solution
% Output:   wopt:   The value of omega that converges to a solution within
%                   the specified tolerance using the least number of
%                   iterations.

% For Problem 5b, I used NE155_HW5_5b(5,'relative',1e-6)

% Initialize k, a vector containing the number of iterations required for
% each value of w tested
k = zeros(1,1999);

% Loop through the SOR code for many values of w, advancing .001 each time.
% Construct the vector k, the number of iterations required for each value
% of w.
for w = .001:.001:1.999
    [x,k(1,round(1000*(w-.001)+1)),errorabs,errorrel] = NE155_HW5_4c(n,w,errormetric,tol);
end

% Multiple values of w will return the same minimum value of k.  I have
% chosen to find them and then average them, taking the mean of these
% values to be the optimal value of w.
tot = 0;
num = 0;
for i = 1:length(k)
    if k(i) == min(k)
        tot = tot + i;
        num = num + 1;
    end
end

% Average the indices of k containing a minimum.
avgind = tot/num;

% Convert this index back to a value of w.
wopt = (avgind-1)/1000 + .001;

end
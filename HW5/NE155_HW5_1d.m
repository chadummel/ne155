function [sol] = NE155_HW5_1d
% Written by Chad Ummel for MATLAB R2012a
% Output:  sol:  a 1xn column vector solution to Ax = b

% Construct A and b using the code from part a.
[A,b] = NE155_HW5_1a(100);

% Solve explicitly using the inverse of A.
sol = A\b;

end
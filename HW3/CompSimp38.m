function [I] = CompSimp38(a,b,n)

% Written by Chad Ummel for NE 155 HW 3
% Inputs:   a,b:	The endpoints of integration.
%           n:      The number of points to use in the integration (so x_0
%                   counts toward n and the final endpoint is x_(n-1)).  n 
%                   must be divisible by three.
% Outputs:  I:  The computed Simpson's rule integral.

f = @(x) x./sqrt(x.^2 - 4);
% This is the function the problem is concerned with.  This code can easily
% be modified to accept any function f as an input.

% f is asymptotic at x = -2 and 2.  Throw an error if the integral [a,b]
% includes either of those points.
if a <= -2 && b >= 2
    error('The interval [a,b] includes -2 and 2, points at which f diverges.')
elseif a <= -2 && b >= -2
    error('The interval [a,b] includes -2, at which f diverges.')
elseif a <= 2 && b >= 2
    error('The interval [a,b] includes 2, at which f diverges.')
else
    
    % Ensure that n is divisible by 3.
    if mod(n,3) == 0

        % Specify the (evenly-spaced) points used in the integration.
        h =(b-a)/(n-1);
        x=a:h:b;
    
        % Calculate the composite Simpson's Rule integral.  This is the
        % formula given on p. 11 of the lecture notes.
        I = h/3*(f(x(1))+4*sum(f(x(2:2:end-1)))+2*sum(f(x(3:2:end-1)))+f(x(end)));    
    else
        
        % If n is not divisible by 3, throw an error.
        error('n must be divisible by 3.')
    end
end

end
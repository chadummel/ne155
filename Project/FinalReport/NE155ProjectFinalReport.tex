\documentclass[final,3p,twocolumn]{elsarticle}

\usepackage{lineno,hyperref}
\usepackage{amsmath}
\usepackage{color}
\usepackage{colortbl}
\usepackage{tabularx}
\usepackage{setspace}

\modulolinenumbers[5]

%%%%%%%%%%%%%%%%%%%%%%%
%% Elsevier bibliography styles
%%%%%%%%%%%%%%%%%%%%%%%
%% To change the style, put a % in front of the second line of the current style and
%% remove the % from the second line of the style you would like to use.
%%%%%%%%%%%%%%%%%%%%%%%

%% Numbered
%\bibliographystyle{model1-num-names}

%% Numbered without titles
%\bibliographystyle{model1a-num-names}

%% Harvard
%\bibliographystyle{model2-names.bst}\biboptions{authoryear}

%% Vancouver numbered
%\usepackage{numcompress}\bibliographystyle{model3-num-names}

%% Vancouver name/year
%\usepackage{numcompress}\bibliographystyle{model4-names}\biboptions{authoryear}

%% APA style
%\bibliographystyle{model5-names}\biboptions{authoryear}

%% AMA style
%\usepackage{numcompress}\bibliographystyle{model6-num-names}

%% `Elsevier LaTeX' style
\bibliographystyle{elsarticle-num}
%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\onehalfspacing

\begin{frontmatter}

\title{DiffusionSolver.m:  A 2D Diffusion Equation Solver}


%% Group authors per affiliation:

\ead{chadummel@berkeley.edu}
\author{Chad Ummel}

\address{University of California, Berkeley, Berkeley, California 94720, USA}

\begin{abstract}

${\tt DiffusionSolverEquation.m}$ is a solver of the two-dimensional diffusion equation implemented in MATLAB. The solver is written specifically for the special case of vacuum left and bottom boundary conditions and reflecting right and top boundaries. In addition to a main solver subroutine, the code contains a number of other useful subroutines. Use of the code is straightforward and test problems produce accurate results.

\end{abstract}

%No more than six keywords.

\end{frontmatter}

\section{Introduction}

A solver of the two-dimensional diffusion equation \cite{lecture}

\begin{equation}
\label{diffequation}
\begin{aligned}
-\frac{\partial}{\partial x}D(x,y)\frac{\partial}{\partial x}\phi(x,y)-\frac{\partial}{\partial y}D(x,y)\frac{\partial}{\partial y}\phi(x,y) \\
+ \Sigma_a(x,y)\phi(x,y) = S(x,y)
\end{aligned}
\end{equation}

${\tt DiffusionSolverEquation.m}$, has been successfully implemented in MATLAB \cite{matlab}. The diffusion equation, shown here in its multidimensional form, 

\begin{equation}
-\vec{\nabla} \cdot \left(D(\vec{r})\vec{\nabla} \phi(\vec{r})\right) + \Sigma_a(\vec{r}) \phi(\vec{r}) = S(\vec{r})
\end{equation}
is derived from the Boltzmann equation for neutron transport:

\begin{equation}
\begin{split}
\frac{1}{v}\frac{\partial \psi(\vec{r}, E, \hat{\Omega}, t)}{\partial t} + \hat{\Omega} \cdot \vec{\nabla} \psi(\vec{r}, E, \hat{\Omega}, t) + \\
\Sigma_t \psi(\vec{r}, E, \hat{\Omega}, t) = \\
\int_{4\pi} d\hat{\Omega}' \int_0^{\infty} dE' \Sigma_s(E', \hat{\Omega}' \rightarrow E, \hat{\Omega}) \psi(\vec{r}, E', \hat{\Omega}', t)  +\\
\frac{\chi(E)}{4\pi} \int_0^{\infty} dE' \nu(E') \Sigma_f(E') \int_{4\pi} d\hat{\Omega}' \psi(\vec{r}, E', \hat{\Omega}', t) +\\
s(\vec{r}, E, \hat{\Omega}, t)
\end{split}
\end{equation}
under the assumptions that the solution is \emph{not} near a \cite{lecture2}

\begin{itemize}
\item void
\item boundary
\item neutron source
\item strong absorber
\end{itemize}

The diffusion equation models the scalar flux, $\phi$ of neutrons through materials with diffusion coefficient $D$, total absorption cross section $\Sigma_a$, and neutron sources defined by the source term $S$. The diffusion equation can be especially useful for describing the behavior of neutrons within a reactor, which is crucial to evaluating the safety and economy of a reactor design. A thorough understanding of neutron transport as governed by the diffusion equation is crucial to innovation and improvement of reactor design.

The solver implements the finite volume method (described in Section \ref{math}) and has specifically been designed to implement the special boundary conditions of a vacuum left and bottom mesh boundaries

\begin{equation}
\label{left}
\phi(x_{min},y) = 0
\end{equation}

\begin{equation}
\label{bottom}
\phi(x,y_{min}) = 0,
\end{equation}
and reflecting top and right boundaries

\begin{equation}
\label{right}
\frac{d}{dx}\phi(x,y)|_{x_{max}} = 0
\end{equation}

\begin{equation}
\label{top}
\frac{d}{dx}\phi(x,y)|_{y_{max}} = 0.
\end{equation}

\section{Mathematics}
\label{math}

The two-dimensional diffusion equation (Eq. \ref{diffequation}) is a linear second-order partial differential equation. A numerical solution $\phi$ to the diffusion equation can be obtained utilizing the Finite Volume Method (FVM), which is briefly described below. A detailed description of FVM can be found in \cite{lecture}.

A two-dimensional spatial mesh is defined, with $(x_i,y_j)$ coordinates designating material discontinuities.  A ``volume'' (actually an area in the 2D formulation) or ``cell,'' $V_{ij}$, enclosed by four points, $x \in [x_{i-1},x_i],\ y \in [y_{j-1},y_j]$ is thus used to designate a specific material with specific properties. The diffusion coefficient, $D_{i,j}$, total absorption cross section, $\Sigma_{a,\ i,j}$, and source term, $S_{i,j}$, are defined as constants for each volume, $x_{i-1}\leq x\leq x_i$, $y_{j-1}\leq y\leq y_j$, i.e., they are ``cell centered.'' We additionally define

\begin{equation}
\delta_i = x_i-x_{i-1}
\end{equation}

\begin{equation}
\epsilon_j = y_j - y_{j-1}
\end{equation}
as the lengths of each cell boundary.

Conversely, the flux $\phi$ is ``edge-centered,'' that is, $\phi_{i,j}$ is defined as constant over the interval $\left(x_i - \frac{\delta_i}{2}\right) \leq x \leq \left(x_i + \frac{\delta_{i+1}}{2}\right)$ and $\left(y_j - \frac{\epsilon_j}{2}\right) \leq y \leq \left(y_j + \frac{\epsilon_{j+1}}{2}\right)$.

Integrating the diffusion equation over each cell (and applying a midpoint approximation) results in the following equation for each point in the mesh, $(x_i,y_j)$ \cite{lecture}

\begin{equation}
\begin{split}
a_L^{ij}\phi_{i-1,j} + a_R^{ij}\phi_{i+1,j} + a_B^{ij}\phi_{i,j-1} + a_T^{ij}\phi_{i,j+1} + a_C^{ij}\phi_{i,j}\\
= S_{ij}
\end{split}
\end{equation}
where

\begin{equation}
a_L^{ij} = -\frac{D_{i,j}\epsilon_j+D_{i,j+1}\epsilon_{j+1}}{2\delta_i}
\end{equation}

\begin{equation}
a_R^{ij} = -\frac{D_{i+1,j}\epsilon_j+D_{i+1,j+1}\epsilon_{j+1}}{2\delta_{i+1}}
\end{equation}

\begin{equation}
a_B^{ij} = -\frac{D_{i,j}\delta_i+D_{i+1,j}\delta_{i+1}}{2\epsilon_j}
\end{equation}

\begin{equation}
a_T^{ij} = -\frac{D_{i,j+1}\delta_i+D_{i+1,j+1}\delta_{i+1}}{2\epsilon_{j+1}}
\end{equation}
where $a_L^{ij}\phi_{i-1,j}$, $a_R^{ij}\phi_{i+1,j}$, $a_B^{ij}\phi_{i,j-1}$, and $a_T^{ij}\phi_{i,j+1}$ designate the left, right, lower, and upper flux into the cell of interest, respectively. Additionally, we define

\begin{equation}
a_C^{ij} = \Sigma_{a,ij} - \left(a_L^{ij} + a_R^{ij}+ a_B^{ij} + a_T^{ij}\right)
\end{equation}
where

\begin{equation}
\begin{split}
\Sigma_{a,\ ij} = \Sigma_{a,\ i,j}V_{i,j} + \Sigma_{a,\ i+1,j}V_{i+1,j} + \\
\Sigma_{a,\ i,j+1}V_{i,j+1} + \Sigma_{a,\ i+1,j+1}V_{i+1,j+1}
\end{split}
\end{equation}
and

\begin{equation}
V_{i,j} = \frac{1}{4}\delta_i\epsilon_j.
\end{equation} 
We also define

\begin{equation}
\begin{split}
S_{ij} = S_{i,j}V_{i,j} + S_{i+1,j}V_{i+1,j} + S_{i,j+1}V_{i,j+1} + \\
S_{i+1,j+1}V_{i+1,j+1}
\end{split}
\end{equation}

Collecting these equations at each grid point forms a matrix equation

\begin{equation}
\mathbf{A\phi} = \mathbf{S}
\end{equation}
where $\mathbf{A}$ is a large matrix constructed from the $a_L$, $a_R$, $a_B$, $a_T$, and $a_C$ values, $\mathbf{S}$ is a column vector containing each $S_{ij}$ value, and $\mathbf{\phi}$ is the scalar flux at each grid point.\cite{lecture}.

For $m$ $x$ grid coordinates and $n$ $y$ grid coordinates, $\mathbf{A}$ is perhaps best visualized as an $n\times n$ tridiagonal matrix, where each entry is an $m\times m$ ``submatrix.'' The submatrices on the diagonal of $\mathbf{A}$ are themselves tridiagonal, containing $a_C$ entries on the main diagonal and $a_L$ and $a_R$ on the lower and upper tridiagonals, respectively. The lower diagonal submatrix entries of $\mathbf{A}$ are themselves diagonal, containing entries of $a_B$ and the upper diagonal submatrix entries of $\mathbf{A}$ contain diagonal entries of $a_T$. For a $3\times 3$ grid, the resulting $9\times 9$ matrix $\mathbf{A}$ is

\begin{equation}
\tiny
\begin{pmatrix}
\begin{pmatrix}
a_{C}^{00} & a_{R}^{00} & 0 \\
a_{L}^{10} & a_{C}^{10} & a_{R}^{10} \\
0            & a_{L}^{20} & a_{C}^{20}
\end{pmatrix} 
&
\begin{pmatrix}
a_{T}^{00} & 0 & 0 \\
0 & a_{T}^{10} & 0 \\
0 & 0 & a_{T}^{20}
\end{pmatrix}
&
\begin{pmatrix}
 & & \\
 & 0 & \\
 & & 
\end{pmatrix} \\
%--------------------
\begin{pmatrix}
a_{B}^{01} & 0 & 0 \\
0 & a_{B}^{11} & 0 \\
0 & 0 & a_{B}^{21}
\end{pmatrix}
&
\begin{pmatrix}
a_{C}^{01} & a_{R}^{01} & 0 \\
a_{L}^{11} & a_{C}^{11} & a_{R}^{11} \\
0            & a_{L}^{21} & a_{C}^{21}
\end{pmatrix}
&
\begin{pmatrix}
a_{T}^{01} & 0 & 0 \\
0 & a_{T}^{11} & 0 \\
0 & 0 & a_{T}^{21}
\end{pmatrix} \\
%--------------------
\begin{pmatrix}
 & & \\
 & 0 & \\
 & & 
\end{pmatrix} &
\begin{pmatrix}
a_{B}^{02} & 0 & 0 \\
0 & a_{B}^{12} & 0 \\
0 & 0 & a_{B}^{22}
\end{pmatrix}
&
\begin{pmatrix}
a_{C}^{02} & a_{R}^{02} & 0 \\
a_{L}^{12} & a_{C}^{12} & a_{R}^{12} \\
0            & a_{L}^{22} & a_{C}^{22}
\end{pmatrix} \\
\end{pmatrix}
\end{equation}

\subsection{Boundary conditions}
\label{bounds}

The boundary conditions result in defined values for $a_L$, $a_R$, $a_B$, $a_T$, $a_C$, and $S$ at the boundaries. For the left vacuum boundary condition (Eq. \ref{left}),

\begin{equation}
a_R^{0j} = a_B^{0j} = a_T^{0j} = 0,
\end{equation}

\begin{equation}
a_C^{0j} = 1,
\end{equation}

\begin{equation}
S_{0j} = 0,
\end{equation}
and

\begin{equation}
a_L^{1j} = 0,
\end{equation}

Similarly, for the bottom vacuum boundary condition (Eq. \ref{bottom}), we set

\begin{equation}
a_L^{i0} = a_R^{i0} = a_T^{i0} = 0,
\end{equation}

\begin{equation}
a_C^{i0} = 1,
\end{equation}

\begin{equation}
S_{i0} = 0,
\end{equation}
and

\begin{equation}
a_B^{i1} = 0,
\end{equation}

For the right reflecting boundary condition (Eq. \ref{right}),

\begin{equation}
a_R^{nj} = 0
\end{equation}

\begin{equation}
a_L^{nj} = -\frac{D_{n,j}\epsilon_j + D_{n,j+1}\epsilon_{j+1}}{2\delta_n}
\end{equation}

\begin{equation}
a_B^{nj} = -\frac{D_{n,j}\delta_n}{2\epsilon_j}
\end{equation}

\begin{equation}
a_T^{nj} = -\frac{D_{n,j+1}\delta_n}{2\epsilon_{j+1}}
\end{equation}

Finally, for the top reflecting boundary condition (Eq. \ref{top}) \cite{boundlecture},

\begin{equation}
a_T^{im} = 0
\end{equation}

\begin{equation}
a_L^{im} = -\frac{D_{i,m}\epsilon_m}{2\delta_i}
\end{equation}

\begin{equation}
a_B^{im} = -\frac{D_{i,m}\delta_i + D_{i+1,m}\delta_{i+1}}{2\epsilon_m}
\end{equation}

\begin{equation}
a_R^{im} = -\frac{D_{i+1,m}\epsilon_m}{2\delta_{i+1}}
\end{equation}

\subsection{Successive Over Relaxation Iterative Linear System Solution}

The matrix equation $\mathbf{A\phi} = \mathbf{S}$ must be solved numerically. Iterative linear system solvers are most commonly used in practice \cite{sorlecture}. One such iterative method is the Successive Over Relaxation (SOR) method. The SOR method was selected for use in the diffusion equation solver code as it was found to converge to a solution in fewer iterations than alternative methods. The SOR method uses the following algorithm \cite{sorlecture}

\begin{equation}
\label{SOReq}
\begin{split}
\phi_i^{(k+1)} = (1-w)\phi_i^{(k)} + \\
\frac{w}{\mathbf{A}_{ii}}\left(\mathbf{S}_i - \sum_{j=1}^{i-1}\mathbf{A}_{ij}\phi_j^{(k+1)} - \sum_{j=i+1}^{n}\mathbf{A}_{ij}\phi_j^{(k)} \right)
\end{split}
\end{equation}
where the superscripts of $\phi$ represent the result the iteration number. Note that the solution incorporates previous indices of the same iteration vector, $\phi^{(k+1)}$. $w$ is a weighting value between 0 and 2. The optimal value of $w$ is problem dependent and as such it is not practical to search for the optimal value of $w$ each time the code is executed \cite{sorlecture}.

\section{Algorithms}

The solver code is an object-oriented class implemented in MATLAB R2012a. The class contains numerous subroutines or \emph{methods}, detailed below.

\begin{itemize}
	\item{ ${\tt PrintVersionData}$} A method that prints the name of the code, ``NE 150 Diffusion Equation Solver,'' the version number, the code author name, and the date and time of execution to screen and a string output file.
	\item{${\tt InputEcho}$} A method that echoes each of the inputs to an outputted cell file.
	\item{${\tt InputCheck}$} A method that checks the dimensions of each input to ensure that they are consistent for each input (i.e. $\mathbf{D}$, $\mathbf{\Sigma_a}$, and $\mathbf{S}$ are the same size and have the same $x$ dimensions as $\delta$ and the same $y$ dimensions as $\epsilon$). If the dimensions are not consistent, the subroutine prints an error to the screen and halts the execution of the code.
	\item{${\tt Solver}$} The diffusion equation solver itself
	\item{${\tt SOR}$} A method to perform the SOR iteration.
\end{itemize}

\subsection{${\tt Solver}$ Subroutine}
The Solver method first calls the ${\tt InputCheck}$ method. If no error is thrown, the method proceeds, constructing the $a_L$, $a_R$, $a_B$, $a_T$, $a_C$ values and $S$ vector entries. The method then applies the boundary condition results as specified in Section \ref{bounds}. These are then arranged into the matrix $\mathbf{A}$. The method then calls the separate ${\tt SOR}$ method and reformats the outputted $\phi$ vector back into a matrix representing the flux at each grid point.

\subsection{${\tt SOR}$ Subroutine}
The ${\tt SOR}$ method code creates a matrix $\mathbf{\phi}$, with each successive row representing the result of each SOR iteration, as defined in Eq. \ref{SOReq}. The weight, $w$, is set to 1.1. The iteration continues until the relative error is less than a specified tolerance of $1\times10^{-8}$.

\begin{equation}
err_{rel} = \frac{||\phi^{(k+1)}-\phi^k||}{\phi^{(k+1)}} < 1\times10^{-8}
\end{equation}

\section{Code Use}

The ${\tt DiffusionSolver}$ code is operated via inputs on the MATLAB command line. First, to define the diffusion equation solver class, enter

\noindent ${\tt obj = DiffusionSolver(x,y,D,\Sigma_a,S);}$

\noindent This defines the object, ${\tt obj}$. ${\tt x}$ and ${\tt y}$ are vectors designating the $x$ and $y$ coordinates of the cell boundaries. These are converted to $\delta$ and $\epsilon$ by the code automatically. ${\tt D}$, ${\tt \Sigma_a}$, and ${\tt S}$ are the diffusion coefficient, total absorption cross section, and source term for each cell. These are arranged how one would view them visually, so the diffusion coefficient in the top left cell is designated by ${\tt D(1,1)}$. The code automatically reformats the matrices so that the first indices represent the bottom left cell, i.e., increasing $y$ indices correspond to increasing height.

Once ${\tt obj}$ is defined, the other methods of the code can be called.

\subsection{PrintVersionData}

To view the code name, version number, the author name, and the date and time of execution, enter

${\tt PrintVersionData(obj)}$

An example output to the command line is

\noindent ${\tt NE\ 155\ Diffusion\ Equation\ Solver,\ Version}$
\noindent${\tt 2.3,\ Written\ by\ Chad\ Ummel,\ 10-May-2016}$
\noindent${\tt 07:08:14}$

\subsection{InputEcho}

Entering

\noindent${\tt [delta,epsilon,D,Sigma_a,S] = InputEcho(obj);}$

\noindent will output the reformatted inputs to the general MATLAB workspace.

\subsection{InputCheck}

Entering

\noindent${\tt InputCheck(obj);}$

\noindent will execute the ${\tt InputCheck}$ method. If the input dimensions are not consistent, an error,

\noindent${\tt Input\ dimensions\ do\ not\ match.}$

\noindent is displayed on the command line. Additionally, the method checks to ensure that ${\tt x}$ and ${\tt y}$ values are increasing. If this is not true, the error 

\noindent${\tt x\ \text{(or $y$)}\ coordinates\ must\ be\ increasing.}$

\noindent is displayed.

\subsection{Solver}

The diffusion equation solver method is executed by entering

\noindent ${\tt Solver(obj)}$

\noindent on the command line. This outputs a solution matrix, ${\tt \phi}$, giving the flux at each grid point. ${\tt \phi}$ is arranged visually, that is ${\tt \phi(1,1)}$ gives the flux at the top left point.

\subsection{SOR}

The ${\tt SOR}$ method is not intended to be used alone.

\section{Test Problems and Results}

For the first test, we define a $3\times 3$ grid from $-0.1\leq x\leq 0.1$, $-0.1\leq y\leq 0.1$, with $D = 1$, $\Sigma_a = 0.2$, and $S = 8$ in each cell:

\noindent ${\tt x = [-.1,0,.1];}$

\noindent ${\tt y = [-.1,0,.1];}$

\noindent ${\tt D = ones(2,2);}$

\noindent ${\tt \Sigma_a = .2*ones(2,2);}$

\noindent ${\tt S = 8*ones(2,2);}$

\noindent The expected result,

\begin{equation}
\tt{\phi =}\\
\begin{pmatrix}
\tt{0} & \tt{0.0699} & \tt{0.0898} \\
\tt{0} & \tt{0.0549} & \tt{0.0699} \\
\tt{0} & \tt{0} & \tt{0} \\
\end{pmatrix} \\
\end{equation}
is obtained exactly.

For the second test, a $(3\times 2)$ grid was used with $-0.1\leq x\leq 0.1$, $0\leq y\leq 0.1$, $D = 1$, $\Sigma_1 = 0.2$, and $S = 2$ in the left cell, and $D = 2$, $\Sigma_a = 0.3$, and $S = 9$ in the right cell:

\noindent ${\tt x = [-.1,0,.1];}$

\noindent ${\tt y = [0,.1];}$

\noindent ${\tt D = [1,2];}$

\noindent ${\tt \Sigma_a = [.2,.3];}$

\noindent ${\tt S = [8,9];}$

\noindent Again, the expected result

\begin{equation}
\tt{\phi =}\\
\begin{pmatrix}
\tt{0} & \tt{0.0215} & \tt{0.0220} \\
\tt{0} & \tt{0} & \tt{0} \\
\end{pmatrix} \\
\end{equation}
is obtained exactly.

A final test was conducted with a $(2\times 3)$ grid, with with $0\leq x\leq 0.1$, $-0.1\leq y\leq 0.1$, $D = 1$, $\Sigma_1 = 0.2$, and $S = 2$ in the top cell, and $D = 2$, $\Sigma_a = 0.3$, and $S = 9$ in the bottom cell:

\noindent ${\tt x = [0,.1];}$

\noindent ${\tt y = [-.1,0,.1];}$

\noindent ${\tt D = [1;2];}$

\noindent ${\tt \Sigma_a = [.2;.3];}$

\noindent ${\tt S = [8;9];}$

\noindent Again, the expected result is obtained:

\begin{equation}
\tt{\phi =}\\
\begin{pmatrix}
\tt{0} & \tt{0.0295}\\
\tt{0} & \tt{0.0191}\\
\tt{0} & \tt{0}\\
\end{pmatrix} \\
\end{equation}

\section{Conclusion}

The ${\tt DiffusionSolverEquation.m}$ solver of the two-dimensional neutron diffusion equation contains an accurate and effective solver algorithm as well as a variety of other useful subroutines. Test problems have produced accurate results and all subroutines function as intended.

\section*{References}

\bibliography{project.bib}

\end{document}
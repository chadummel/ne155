classdef DiffusionSolver
% Written by Chad Ummel for the NE 155 2-D Diffusion Equation Solver
% Project

    properties (Access = public)
        delta       % x length of each cell boundary
        epsilon     % y length of each cell boundary
        D           % The diffusion coefficient in each cell
        Sigma_a     % The total absorption cross section in each cell
        S           % The neutron source term in each cell
        codename    % The code name, "NE 155 Diffusion Equation Solver"
        version     % The code version
        author      % The code author, Chad Ummel
        A           % A matrix in the A*phi = b matrix equation sent to the
                    % SOR iterative solver
        b           % b matrix in the A*phi = b matrix equation sent to the
                    % SOR iterative solver
    end
    
    methods
        
        function obj = DiffusionSolver(x,y,D,Sigma_a,S)
            % Purpose:  Constructor for Diffusion Equation Solver objects
            % Inputs:   x       -- x coordinates of cell boundaries,
            %                      arranged in increasing order
            %           y       -- y coordinates of cell boundaries,
            %                      arranged in increasing order
            %           D       -- The diffusion coefficient in each cell,
            %                      arranged visually, i.e., the diffusion 
            %                      coefficient in the top left cell
            %                      (xmin,ymax) is the top left entry in the
            %                      array
            %           Sigma_a -- The total absorption cross section in
            %                      each cell, arranged visually, i.e., the
            %                      diffusion coefficient in the top left
            %                      cell (xmin,ymax) is the top left entry
            %                      in the array
            %           S       -- The neutron source term in each cell,
            %                      arranged visually, i.e., the source term
            %                      in the top left cell (xmin,ymax) is the
            %                      top left entry in the array
            % Output:   obj -- Diffusion Equation Solver object
            
            % D, Sigma_a, and S are flipped and transposed so that the
            % first array index references ascending x coordinates and the
            % second index references ascending y coordinates
            obj.D = flipud(D)';
            obj.Sigma_a = flipud(Sigma_a)';
            obj.S = flipud(S)';
            
            % Construct the delta and epsilon vectors from the inputted x
            % and y cell boundary coordinates
            obj.delta = zeros(1,length(x)-1);
            obj.epsilon = zeros(1,length(y)-1);
            for i = 1:length(x)-1
                obj.delta(i) = x(i+1) - x(i);
            end
            for j = 1:length(y)-1
                obj.epsilon(j) = y(j+1) - y(j);
            end
            
            % Version Data
            obj.codename = 'NE 155 Diffusion Equation Solver';
            obj.version = '2.3';
            obj.author = 'Chad Ummel';
            
            % Initialize A and b
            obj.A = [];
            obj.b = [];
        end
        
        function VersionData = PrintVersionData(obj)
            % Purpose:  Output useful information about code.
            % Input:    obj -- Diffusion Equation Solver class
            % Output:   VersionData -- A string containing the code name,
            %                          version, author name, and date and
            %                          time of execution.
            
            VersionData = [obj.codename,', Version ',obj.version,', Written by ',obj.author,', ',datestr(now)];
        end
        
        function [delta,epsilon,D,Sigma_a,S] = InputEcho(obj)
            % Purpose: Echo the formatted inputs to an output file
            % Input:    obj -- Diffusion Equation Solver object
            % Outputs:  delta   -- x length of each cell (formatted from
            %                      the x cell boundary coordinates input)
            %           epsilon -- y length of each cell (formatted from
            %                      the y cell boundary coordinates input)
            %           D       -- The formatted diffusion coefficient
            %                      array
            %           Sigma_a -- The formatted total absorption cross
            %                      section array
            %           S       -- The formatted neutron source array
            
            delta = obj.delta;
            epsilon = obj.epsilon;
            D = obj.D;
            Sigma_a = obj.Sigma_a;
            S = obj.S;
        end
        
        function InputCheck(obj)
            % Purpose:  Check the dimensions of each input to ensure that
            %           they are consistent.  If not, throw an error and
            %           terminate the code.
            % Input:    obj -- the Diffusion Equation Solver object

            % Check to ensure delta and epsilon contain only positive
            % values
            for i = 1:length(obj.delta)
                if obj.delta(i) <= 0
                    error('x coordinates must be increasing.')
                end
            end
            for j = 1:length(obj.epsilon)
                if obj.epsilon(j) <= 0
                    error('y coordinates must be increasing.')
                end
            end
            
            % Check the remaining inputs
            if size(obj.D) == size(obj.Sigma_a)
                if size(obj.D) == size(obj.S)
                    [sizex,sizey] = size(obj.D);
                    if length(obj.delta) == sizex;
                        if length(obj.epsilon) == sizey;
                        else
                            error('Input dimensions do not match.')
                        end
                    else
                        error('Input dimensions do not match.')
                    end
                else
                    error('Input dimensions do not match.')
                end
            else
                error('Input dimensions do not match.')
            end
        end
        
        function [phi,inputs] = Solver(obj)
            % Purpose:  Solve the two-dimensional diffusion equation for
            %           the given inputs on the dimensions, diffusion
            %           coefficient, total absorption cross section, and
            %           neutron source term in each cell with top and right
            %           reflecting boundaries and bottom and left vacuum
            %           boundaries
            % Input:    obj -- Diffusion Equation Solver class
            % Output:   phi -- The flux at each x,y point, arranged
            %                  visually, i.e., the flux at the top left
            %                  point (xmin,ymax) is the top left entry in
            %                  the array 
            
            PrintVersionData(obj)
            [delta,epsilon,D,Sigma_a,S] = InputEcho(obj);
            inputs = {delta,epsilon,D,Sigma_a,S};
            
            % Call the InputCheck method to check the dimensions of the
            % inputs before running
            InputCheck(obj);            

            % Record the number of cells in the x and y directions
            xcells = length(obj.delta)+1;
            ycells = length(obj.epsilon)+1;

            % Initialize Sigma_a (combined), S (combined), aL, aR, aT, aB,
            % and aC
            Sigma_acomb = zeros(xcells-2,ycells-2);
            Scomb = zeros(xcells-2,ycells-2);
            aL = zeros(xcells,ycells);
            aR = zeros(xcells,ycells);
            aT = zeros(xcells,ycells);
            aB = zeros(xcells,ycells);
            aC = zeros(xcells,ycells);
            
            % Compute Sigma_a (combined) and S (combined) for each mesh
            % point

            Sigma_acomb(1,1) = obj.Sigma_a(1,1)*.25*obj.delta(1)*obj.epsilon(1);
            Scomb(1,1) = obj.S(1,1)*.25*obj.delta(1)*obj.epsilon(1);
            Sigma_acomb(xcells,ycells)=obj.Sigma_a(xcells-1,ycells-1)*.25*obj.delta(xcells-1)*obj.epsilon(ycells-1);
            Scomb(xcells,ycells)=obj.S(xcells-1,ycells-1)*.25*obj.delta(xcells-1)*obj.epsilon(ycells-1);

            for i = 0:xcells-1
                for j = 0:ycells-1
                    if i == 0 && j == 0
                    elseif i == xcells-1 && j == ycells-1
                    elseif i == 0 && j ~= 0 && j ~= ycells-1
                        V2 = .25*obj.delta(i+1)*obj.epsilon(j);
                        V4 = .25*obj.delta(i+1)*obj.epsilon(j+1);
                        Sigma_acomb(i+1,j+1)=obj.Sigma_a(i+1,j)*V2+obj.Sigma_a(i+1,j+1)*V4;
                        Scomb(i+1,j+1) = obj.S(i+1,j)*V2+obj.S(i+1,j+1)*V4;
                    elseif i ~= 0 && i ~= xcells-1 && j == 0
                        V3 = .25*obj.delta(i)*obj.epsilon(j+1);
                        V4 = .25*obj.delta(i+1)*obj.epsilon(j+1);
                        Sigma_acomb(i+1,j+1)=obj.Sigma_a(i,j+1)*V3+obj.Sigma_a(i+1,j+1)*V4;
                        Scomb(i+1,j+1) = obj.S(i,j+1)*V3+obj.S(i+1,j+1)*V4;
                    elseif i == xcells-1 && j ~= 0 && j ~= ycells-1
                        V1 = .25*obj.delta(i)*obj.epsilon(j);
                        V3 = .25*obj.delta(i)*obj.epsilon(j+1);
                        Sigma_acomb(i+1,j+1)=obj.Sigma_a(i,j)*V1+obj.Sigma_a(i,j+1)*V3;
                        Scomb(i+1,j+1) = obj.S(i,j)*V1 + obj.S(i,j+1)*V3;
                    elseif i ~= 0 && i~=xcells-1 && j == ycells-1
                        V1 = .25*obj.delta(i)*obj.epsilon(j);
                        V2 = .25*obj.delta(i+1)*obj.epsilon(j);
                        Sigma_acomb(i+1,j+1)=obj.Sigma_a(i,j)*V1+obj.Sigma_a(i+1,j)*V2;
                        Scomb(i+1,j+1) = obj.S(i,j)*V1 + obj.S(i+1,j)*V2;
                    elseif i ~= 0 && i~=xcells-1 && j ~= 0 && j ~= ycells-1
                        V1 = .25*obj.delta(i)*obj.epsilon(j);
                        V2 = .25*obj.delta(i+1)*obj.epsilon(j);
                        V3 = .25*obj.delta(i)*obj.epsilon(j+1);
                        V4 = .25*obj.delta(i+1)*obj.epsilon(j+1);
                        Sigma_acomb(i+1,j+1)=obj.Sigma_a(i,j)*V1+obj.Sigma_a(i+1,j)*V2+obj.Sigma_a(i,j+1)*V3+obj.Sigma_a(i+1,j+1)*V4;
                        Scomb(i+1,j+1)=obj.S(i,j)*V1+obj.S(i+1,j)*V2+obj.S(i,j+1)*V3+obj.S(i+1,j+1)*V4;
                    end
                end
            end
        

            % Form aL, aR, aT, and aB
            for i = 1:xcells-2
                for j = 1:ycells-2
                    aL(i+1,j+1)=-(obj.D(i,j)*obj.epsilon(j)+obj.D(i,j+1)*obj.epsilon(j+1))/(2*obj.delta(i));
                    aR(i+1,j+1)=-(obj.D(i+1,j)*obj.epsilon(j)+obj.D(i+1,j+1)*obj.epsilon(j+1))/(2*obj.delta(i+1));
                    aT(i+1,j+1)=-(obj.D(i,j+1)*obj.delta(i)+obj.D(i+1,j+1)*obj.delta(i+1))/(2*obj.epsilon(j+1));
                    aB(i+1,j+1)=-(obj.D(i,j)*obj.delta(i)+obj.D(i+1,j)*obj.delta(i+1))/(2*obj.epsilon(j));
                    aC(i+1,j+1)=Sigma_acomb(i+1,j+1)-aL(i+1,j+1)-aR(i+1,j+1)-aB(i+1,j+1)-aT(i+1,j+1);
                end
            end
            
            % Find the size of S
            [Sx,Sy] = size(obj.S);

            % Impose boundary conditions
            
            % Apply right reflecting condition
            for j = 0:ycells-1
                aR(end,j+1) = 0;
                if j == 0
                    aL(end,j+1)=-obj.D(end,j+1)*obj.epsilon(j+1)/(2*obj.delta(end));
                    aB(end,j+1) = 0;
                    aT(end,j+1)=-obj.D(end,j+1)*obj.delta(end)/(2*obj.epsilon(j+1));
                elseif j == ycells-1
                    aL(end,j+1)=-obj.D(end,j)*obj.epsilon(j)/(2*obj.delta(end));
                    aB(end,j+1)=-obj.D(end,j)*obj.delta(end)/(2*obj.epsilon(j));
                    aT(end,j+1) = 0;
                else
                    aL(end,j+1)=-(obj.D(end,j)*obj.epsilon(j)+obj.D(end,j+1)*obj.epsilon(j+1))/(2*obj.delta(end));
                    aB(end,j+1)=-obj.D(end,j)*obj.delta(end)/(2*obj.epsilon(j));
                    aT(end,j+1)=-obj.D(end,j+1)*obj.delta(end)/(2*obj.epsilon(j+1));
                end
                aC(end,j+1)=Sigma_acomb(end,j+1)-aL(end,j+1)-aB(end,j+1)-aT(end,j+1);
            end

            % Apply top reflecting condition
            for i = 0:xcells-1
                aT(i+1,end) = 0;
                if i == 0
                    aL(i+1,end) = 0;
                    aB(i+1,end)=-obj.D(i+1,end)*obj.delta(i+1)/(2*obj.epsilon(end));
                    aR(i+1,end)=-obj.D(i+1,end)*obj.epsilon(end)/(2*obj.delta(i+1));
                elseif i == xcells-1
                    aL(i+1,end)=-obj.D(i,end)*obj.epsilon(end)/(2*obj.delta(i));
                    aB(i+1,end)=-obj.D(i,end)*obj.delta(i)/(2*obj.epsilon(end));
                    aR(i+1,end) = 0;
                else
                    aL(i+1,end)=-obj.D(i,end)*obj.epsilon(end)/(2*obj.delta(i));
                    aB(i+1,end)=-(obj.D(i,end)*obj.delta(i)+obj.D(i+1,end)*obj.delta(i+1))/(2*obj.epsilon(end));
                    aR(i+1,end)=-obj.D(i+1,end)*obj.epsilon(end)/(2*obj.delta(i+1));
                end
                aC(i+1,end)=Sigma_acomb(i+1,end)-aL(i+1,end)-aB(i+1,end)-aR(i+1,end);
            end

            % Apply left vacuum condition
            phiL = 0;
            aC(1,:) = 1;
            aR(1,:) = 0;
            aT(1,:) = 0;
            aB(1,:) = 0;
            Scomb(1,:) = phiL;
            Scomb(2,:) = Scomb(2,:)-aL(2,:)*phiL;
            aL(2,:) = 0;

            % Apply bottom vacuum condition
            phiB = 0;
            aC(:,1) = 1;
            aL(:,1) = 0;
            aR(:,1) = 0;
            aT(:,1) = 0;
            Scomb(:,1) = phiB;
            Scomb(:,2) = Scomb(:,2) - aB(:,2)*phiB;
            aB(:,2) = 0;

            % Initialize the matrix A as an empty cell of size nxn. A will
            % ultimately be an nxn matrix with each of its entries an mxm
            % "submatrix" (where m is the number of x mesh coordinates and
            % n is the number of y mesh coordinates.  We will insert these
            % nxn submatrices into each cell entry.
            A = cell(ycells,ycells); 

            % Matrices not on the tridiagonal entries of A are zero.
            % Initialize all submatrices as 0 matrices.
            A(:,:) = {zeros(xcells,xcells)};

            % Fill out the lower tridiagonal entries of A with diagonal
            % matrices composed of aB values
            for j = 1:ycells-1
                A{j+1,j} = diag(aB(:,j+1));
            end

            % Fill out the upper tridiagonal entries of A with diagonal
            % matrices composed of aT values
            for j = 1:ycells-1
                A{j,j+1} = diag(aT(:,j));
            end


            % Fill out the diagonal entries of A.
            for j = 1:ycells
    
                % Form the diagonal entries of each submatrix from aC
                % values.
                A{j,j} = diag(aC(:,j));
    
                % Form the lower and upper tridiagonal entries of each
                % submatrix from aL and aR values.
                for i = 1:xcells-1
                    A{j,j}(i,i+1) = aR(i,j);
                end
                for i = 1:xcells-1
                    A{j,j}(i+1,i) = aL(i+1,j);
                end
            end

            % Convert the cell A to a matrix
            A = cell2mat(A);

            % Format S into a vector
            Slong = [];
            for j = 1:Sy+1
                Slong = [Slong;Scomb(:,j)];
            end

            % Phi will become a matrix, with each row representing the
            % result of one iteration of the SOR mehod, i.e., top row of
            % phi will be phi^(0) which will be equal to the zero vector.
            % The second row of phi will be phi^(1) (the result of the
            % second iteration) and so forth.
            
            obj.A = A;
            obj.b = Slong;
            philong = SOR(obj);
            
            phi = [];
            for j = 1:Sy+1
                phi = [phi,philong((j-1)*(Sx+1)+1:j*(Sx+1),1)];
            end

            phi = flipud(phi');
            
        end

        function x = SOR(obj)

            % Matrix Equation Iterative Solver
            % Based on SOR iterative solver written for HW 5, Problem 4c
            x = zeros(1,length(obj.b));
            n = length(x);

            % Set the value of the weight, w (This may be adjusted later).
            w = 1.1;

            % Initialize the relative error as infinity.
            errorrel = Inf;

            % Loop until we meet a tolerance of less than 1e-6
            while errorrel > 1e-8
    
                % Initialize the iterated x as a row of zeros under the
                % previous x.
                x = [x;zeros(1,n)];
    
                % Loop through each element of x
                for i = 1:n
        
                    % Initialize the second and third terms of the Gauss
                    % Seidel method algorithm
                    sumleft = 0;
                    sumright = 0;
        
                    % Compute the terms.  Note that we use x(end,j) for sum
                    % left instead of x(end-1,j), which we used for the
                    % Jacobi method.
                    for j = 1:i-1
                        sumleft = sumleft + obj.A(i,j)*x(end,j);
                    end
                    for j = i+1:n
                        sumright = sumright + obj.A(i,j)*x(end-1,j);
                    end
        
                    % Compute each element of the newly formed x
                    x(end,i)=(1-w)*x(end-1,i)+w*(obj.b(i,1)-sumleft-sumright)/obj.A(i,i);
                end
    
                % Recompute the error
                errorrel=sqrt((sum((x(end,:)-x(end-1,:)).^2))/sum((x(end,:)).^2));
  
            end

            % Remove the solution (the last row of the x matrix) and
            % transpose it to a column vector. Then reformat into a matrix
            % representing the flux at each 2D point.
            x = x(end,:)';
        end
        
    end
    
end
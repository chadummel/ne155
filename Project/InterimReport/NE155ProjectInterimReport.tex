\documentclass[final,3p,twocolumn]{elsarticle}

\usepackage{lineno,hyperref}
\usepackage{amsmath}
\usepackage{color}
\usepackage{colortbl}
\usepackage{tabularx}
\usepackage{setspace}

\modulolinenumbers[5]

%%%%%%%%%%%%%%%%%%%%%%%
%% Elsevier bibliography styles
%%%%%%%%%%%%%%%%%%%%%%%
%% To change the style, put a % in front of the second line of the current style and
%% remove the % from the second line of the style you would like to use.
%%%%%%%%%%%%%%%%%%%%%%%

%% Numbered
%\bibliographystyle{model1-num-names}

%% Numbered without titles
%\bibliographystyle{model1a-num-names}

%% Harvard
%\bibliographystyle{model2-names.bst}\biboptions{authoryear}

%% Vancouver numbered
%\usepackage{numcompress}\bibliographystyle{model3-num-names}

%% Vancouver name/year
%\usepackage{numcompress}\bibliographystyle{model4-names}\biboptions{authoryear}

%% APA style
%\bibliographystyle{model5-names}\biboptions{authoryear}

%% AMA style
%\usepackage{numcompress}\bibliographystyle{model6-num-names}

%% `Elsevier LaTeX' style
\bibliographystyle{elsarticle-num}
%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\onehalfspacing

\begin{frontmatter}

\title{A 2D Diffusion Solver: Interim Report}


%% Group authors per affiliation:

\ead{chadummel@berkeley.edu}
\author{Chad Ummel}

\address{University of California, Berkeley, Berkeley, California 94720, USA}

%No more than six keywords.

\end{frontmatter}

\section{Overview}

A solver of the two-dimensional Diffusion Equation (Equation \ref{diffequation}) \cite{lecture} is currently in production.

\begin{equation}
\label{diffequation}
\begin{aligned}
-\frac{\partial}{\partial x}D(x,y)\frac{\partial}{\partial x}\phi(x,y)-\frac{\partial}{\partial y}D(x,y)\frac{\partial}{\partial y}\phi(x,y) \\
+ \Sigma_a(x,y)\phi(x,y) = S(x,y)
\end{aligned}
\end{equation}

The code (found at bitbucket.org/chadummel/ne155) is written in MATLAB and will utilize the Finite Volume numerical method. The boundary conditions on the solution are as follows: the top and right faces are reflecting boundaries, that is

\begin{equation}
\frac{d}{dx}\phi(x,y)|_{x=a} = 0
\end{equation}

\begin{equation}
\frac{d}{dx}\phi(x,y)|_{y=b} = 0
\end{equation}
and the bottom and left faces are vacuum boundaries, that is

\begin{equation}
\phi(0-2D,y) = 0
\end{equation}

\begin{equation}
\phi(x,0-2D) = 0.
\end{equation}

The code accepts as inputs three arrays and two vectors detailing the properties of a mesh of finite volumes over which to solve the Diffusion Equation.  The first input, $\mathbf{D}$ is an $m\times n$ array giving the diffusion coefficient of materials contained in the center each of $m\times n$ volumes, $V_{i,j}$.  The second input, $\mathbf{\Sigma_a}$ is also an $m\times n$ matrix carrying the macroscopic neutron absorption cross section for the materials at the center of each volume. The third input, $\mathbf{S}$ is the ``source'' term, an $m\times n$ array describing the production of neutrons at the center of each volume. Because $\mathbf{D}$, $\mathbf{\Sigma_a}$, and $\mathbf{S}$ are constant within each volume or \emph{cell}, they are said to be \emph{cell-centered}.

Two input vectors, $\delta$ and $\epsilon$ give the $x$ and $y$ dimensions of each cell, respectively.

\section{Mathematics}

The solution to the 2D Diffusion Equation will have the form

\begin{equation}
\mathbf{A\phi} = \mathbf{S}
\end{equation}
where $\mathbf{A}$ is a large matrix, $\mathbf{S}$ is the source column vector, and $\mathbf{\phi}$ is the solution vector. \cite{lecture}

Currently, the solver the 2D Diffusion Equation under fixed boundary conditions, that is

\begin{equation}
\phi(-a,y) = \Phi_L
\end{equation}

\begin{equation}
\phi(a,y) = \Phi_R
\end{equation}

\begin{equation}
\phi(x,-b) = \Phi_B
\end{equation}

\begin{equation}
\phi(x,b) = \Phi_T
\end{equation}

This can be done for any $n \times n$ grid, so the resulting matrix $\mathbf{A}$ will be a $n^2 \times n^2$ matrix. The code will then be modified to construct $\mathbf{A}$ (and the vector $\mathbf{S}$) for any specified (via an input value) number of volumes.  Currently, the solver can only accept an $n\times n$ mesh of cells. The solver will be modified to accept any rectangular $m\times n$ array of cells.

The matrix $\mathbf{A}$ is constructed by first constructing four smaller $m \times n$ matrices, $\mathbf{a_L}$, $\mathbf{a_R}$, $\mathbf{a_B}$, $\mathbf{a_T}$, and $\mathbf{a_C}$ which are the same size as $\mathbf{D}$, $\mathbf{\Sigma_a}$, and $\mathbf{S}$, using the following equations:

\begin{equation}
a_L^{ij} = -\frac{D_{i,j}\epsilon_j+D_{i,j+1}\epsilon{j+1}}{2\delta_i}
\end{equation}

\begin{equation}
a_R^{ij} = -\frac{D_{i+1,j}\epsilon_j+D_{i+1,j+1}\epsilon{j+1}}{2\delta_{i+1}}
\end{equation}

\begin{equation}
a_B^{ij} = -\frac{D_{i,j}\delta_i+D_{i+1,j}\delta{i+1}}{2\epsilon_j}
\end{equation}

\begin{equation}
a_T^{ij} = -\frac{D_{i,j+1}\delta_i+D_{i+1,j+1}\delta{i+1}}{2\epsilon_{j+1}}
\end{equation}
These give the left, right, lower, and upper flux into the cell of interest, respectively.  Additionally,

\begin{equation}
a_C^{ij} = \Sigma_{a,ij} - \left(a_L^{ij} + a_R^{ij}+ a_B^{ij} + a_T^{ij}\right)
\end{equation}
$\mathbf{A}$ is then constructed using the method outlines in \cite{lecture}.

\section{Algorithms}

The solver code also contains a number of additional features, namely:

\begin{itemize}
	\item A subroutine that prints the name of the code, ``NE 150 Diffusion Equation Solver,'' the version number, the code author name, and the date and time of execution to screen and a string output file.
	\item A subroutine that echoes each of the inputs to an outputted cell file.
	\item A subroutine that checks the dimensions of each input to ensure that they are consistent for each input (i.e. $\mathbf{D}$, $\mathbf{\Sigma_a}$, and $\mathbf{S}$ are the same size and $\delta$ and $\epsilon$ are the same length). If the dimensions are not consistent, the subroutine prints an error to the screen and halts the execution of the code.
\end{itemize}

The code is currently formatted as a standard MATLAB script function. The final product will be a MATLAB object-oriented class, with each subroutine (including the solver code itself) defined as distinct methods within the class definition.

\section{Plans for Completion}
\label{steps}

This section details a list of major milestones to be completed in the construction of the solver code, the goals to be accomplished at each step, and a tentative deadlines by which each step should be completed in order to remain on schedule. The full solver will be completed and presented on May 10, 2016.

\subsection{April 18:  Modify the code to process non-square input matrices}
Currently the code is only able to accept $\mathbf{D}$, $\mathbf{\Sigma_a}$, and $\mathbf{S}$ in the form of $n\times n$ matrices. Thus, the code can currently only process an $n\times n$ array of cells. The code will be modified to process rectangular $m\times n$ arrays of cells.

\subsection{April 20: Implement code to solve the matrix equations using an iterative method}
The large matrix equation formed is best solved iteratively using a method such as Jacobi, Gauss Seidel, or Successive Over Relaxation (SOR) iteration. SOR iteration will be used, as previous coursework indicated that SOR iteration converged faster than Jacobi or Gauss Seidel iteration. The algorithm will iterate until the solution for $\phi$ has reached the given stopping criterion

\begin{equation}
err_{rel} = \frac{||x^{(k+1)}-x^k||}{x^{(k+1)}} < \epsilon
\end{equation}
where $\epsilon$ is the specified tolerance.

\subsection{April 29: Modify code to construct matrix equation for special boundary conditions}
The code constructing the matrix $\mathbf{A}$ (and vector $\mathbf{S}$) will be modified so as to account for the boundary conditions specified by the objective: bottom and left vacuum boundaries and top and right reflecting boundaries.

\subsection{May 2: Reformat the code into an object-oriented class}

The code is currently formatted as a standard MATLAB script function. The final product will be a MATLAB object-oriented class, with each subroutine (including the solver code itself) defined as distinct methods within the class definition.

\subsection{Testing}

A round of testing will be performed with each step to ensure the outputs of each step are accurate.

\section*{References}

\bibliography{project.bib}

\end{document}
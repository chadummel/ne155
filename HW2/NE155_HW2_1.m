function NE155_HW2_1(X)
% Written by Chad Ummel for MATLAB R2012a
% Input:  X, a row array of interpolation points

f = @(x) cos(pi*x/2)+x.^2/2;
% This is the function we were concerned with.  The code can be easily
% modified to accept any function handle as an input.
y = f(X);

% Initialize the interpolating polynomial
P = @(x) 0;
for k = 1:length(X)
    
    L = @(x) 1;
    for i = 1:length(X)
        
        % Create Lagrange polynomials by looping through interpolation
        % points and multiplying existing polynomial by
        % (x-X(i))/(X(k)-X(i))
        if i == k
            l = @(x) 1;
        else
            l = @(x) (x-X(i))/(X(k)-X(i));
        end
        L = @(x) L(x).*l(x);
    end
    
    % Add most recently created Lagrange polynomial to the interpolating
    % polynomial
    P = @(x) P(x) + f(X(k))*L(x);
end

% Plot the interpolation points as black dots
plot(X,y,'k.')
hold

% Plot the orginal function and the interpolation polynomial over 100
% equally-spaced points from -.5 to 4.5
points = linspace(-.5,4.5,100);
plot(points,f(points),'b')
plot(points,P(points),'r')
xlabel('x')
ylabel('y')
title('NucE 155 HW 2 #1')
legend('Points','Actual Function','Interpolating Polynomial')
end
function [phi] = NE155_HW6_2(h)
% Written by Chad Ummel for MATLAB R2012a
% Input:    h:  mesh size (in cm) used for the finite difference method.
%           For Problem 2, use h = .1
% Outputs:  phi:    The solution to the the fixed-source diffusion equation
%                   obtained using the finite difference method
%           A plot of the finite difference solution to the fized-source
%           diffusion equation from [-a,a] plotted against the real
%           solution

a = 4;
n = 2*a/h;
D = 1;
Sigma = .2;
S = 8;

% Construct the matrix

% Initialize
A = zeros(n-1,n-1);
b = h^2*S*ones(n-1,1)/D; % Each entry of b is h^2*S/D
phi = zeros(n-1,1);

% First row
A(1,1) = 2+h^2*Sigma/D;
A(1,2) = -1;

% Middle rows
for i = 2:n-2
    A(i,i-1) = -1;
    A(i,i) = 2+h^2*Sigma/D;
    A(i,i+1) = -1;
end

% Last row
A(n-1,n-2) = -1;
A(n-1,n-1) = 2+h^2*Sigma/D;


% Solve using Gaussian Elimination

% Subtract second row from first

% Remove leftmost entry from rows 2 to 79
for i = 2:(n-1)
    fact = A(i,i-1)/A(i-1,i-1);
    A(i,:) = A(i,:)-fact*A(i-1,:);
    b(i) = b(i) - fact*b(i-1);
end

% Remove rightmost entry from rows 1 to 78
for i = (n-2):-1:1
    fact = A(i,i+1)/A(i+1,i+1);
    A(i,:) = A(i,:) - fact*A(i+1,:);
    b(i) = b(i) - fact*b(i+1);
end

% Solve for phi
for i = 1:(n-1)
    phi(i) = b(i)/A(i,i);
end

% Add boundary conditions--phi_0 = phi_80 = 0 to phi vector
phi = [0;phi;0];

% Plot the result
plot(-4:h:4,phi,'k.','MarkerSize',15)
hold
xlabel('x')
ylabel('\phi(x)')
title('Finite Difference Solution to the Fixed-Source Diffusion Equation')

% Plot real solution (from Question 1b)
C = [exp(a*sqrt(Sigma/D)),exp(-1*a*sqrt(Sigma/D));exp(-1*a*sqrt(Sigma/D)),exp(a*sqrt(Sigma/D))];
d = -1*S*ones(2,1)/Sigma;
c = C\d;
phireal = @(x) c(1)*exp(-1*x*sqrt(Sigma/D))+c(2)*exp(x*sqrt(Sigma/D))+S/Sigma;
fplot(phireal,[-a,a])

legend('Finite Difference Solution','Real Solution')
end
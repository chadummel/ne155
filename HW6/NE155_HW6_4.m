function [phi,k,iter] = NE155_HW6_4
% Written by Chad Ummel for MATLAB R2012a
% Outputs:  phi:    The solution to the the eigenvalue form of the
%                   diffusion equation obtained using the finite difference
%                   method.
%           k:      The eigenvalue k in the eigenvalue form of the
%                   diffusion equation
%           iter:   The number of power iterations required for convergence
%                   to relative errors of k and the two-norm of phi below a
%                   tolerance of 1e-4.
%           A plot of the finite difference solution to the fized-source
%           diffusion equation from [-a,a] plotted against the real
%           solution

% USE ONLY WITH THE MODIFIED NE155_HW5_4c_modified SOR SOLVER CODE.  THE
% ORIGINAL CODE SUBMITTED WITH HW5 WILL NOT WORK


% Set constants

a = 4;
D = 1;
Sigmaa = .7;
vSigmaf = .6;
h = .1;
n = 2*a/h;

% Use the Finite Difference Method for discretization of the spatial
% variable (as in NE155_HW6_2)

% Construct the matrix

% Initialize
A = zeros(n-1,n-1);
b = h^2*vSigmaf*ones(n-1,1); % Each entry of b is (1/k)*h^2*vSigmaf*phi(x)
F = diag(b);

% First row
A(1,1) = 2*D/h^2 + Sigmaa;
A(1,2) = -D/h^2;

% Middle rows
for i = 2:n-2
    A(i,i-1) = -D/h^2;
    A(i,i) = 2*D/h^2 + Sigmaa;
    A(i,i+1) = -D/h^2;
end

% Last row
A(n-1,n-2) = -D/h^2;
A(n-1,n-1) = 2*D/h^2 + Sigmaa;

% Use the Power Method to solve A*phi = (1/k)*F*phi

% Make initial guesses of k = 1 and phi = [1,1,...,1]
k = 1;
phi = ones(n-1,1);

% normalize phi
phi = phi/sqrt(sum(phi.^2));

% Compute the initial fission source
Q = vSigmaf*phi;

% Etablish the tolerance
epsilon = 1e-4;

% Initialize the loop-terminating condition variable and the iteration
% counter
cond = 0;
iter = 0;
while cond == 0
    
    % Use the MODIFIED SOR solver code to compute a solution for phi
    phinew = NE155_HW5_4c_modified(A,Q/k,1.1,'relative',1e-4);
    
    % Calculate new values of Q and k
    Qnew = vSigmaf*phinew;
    knew = sum(Qnew)/sum(Q);
    
    % Check if tolerances are met.  If yes, set cond = 1, which will
    % terminate the loop
    if (knew-k)/knew < epsilon
        if sqrt((phinew-phi).^2)/sqrt(phinew.^2) < epsilon
            cond = 1;
        end
    end
    
    Q = Qnew;
    phi = phinew;
    k = knew;
    
    % Increase iteration counter by 1.
    iter = i+1;
end

% Set phi(-a) = phi(a) = 0
phi = [0;phi;0];

% Plot the result
plot(-a:h:a,phi,'k.','MarkerSize',15)
xlabel('x')
ylabel('\phi(x)')
title('Finite Difference Solution to the Eigenvalue Form of the Diffusion Equation')

end